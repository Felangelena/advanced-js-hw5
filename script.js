const container = document.querySelector("#container");

class Card {
  constructor(header, text, name, email, postId) {
      this.header = header,
      this.text = text,
      this.name = name,
      this.email = email,
      this.postId = postId
  }

  render() {
      const post = document.createElement('div');
      post.innerHTML = `
      <div class="card" data-postId="${this.postId}">
        <div class="card__header">
          <div class="user"><span class="card__user-name">${this.name}</span> | <span class="card__user-email">${this.email}</div>
          <button class="card__btn-delete" title="Delete post">X</button>
        </div>
        <h2 class="card__post-title">${this.header}</h2>
        <p class="card__post-txt">${this.text}</p>
      </div>
      `;
      container.append(post);
  }
}

fetch(('https://ajax.test-danit.com/api/json/users'), {method: 'GET'})
  .then(response => response.json())
  .then(responseUsers => {

    fetch(('https://ajax.test-danit.com/api/json/posts'), {method: 'GET'})
  .then(response => response.json())
  .then(responsePosts => {
    responsePosts.forEach((post) => {
      let card = new Card (post.title, post.body, responseUsers[post.userId-1].name, responseUsers[post.userId-1].email, post.id);
      card.render();
      })
  })
})

container.addEventListener("click", e => {
  const element = e.target;
  if(element.classList.contains("card__btn-delete")) {
    const card = element.closest(".card");
    const postId = card.dataset.postid;
  fetch((`https://ajax.test-danit.com/api/json/posts/${postId}`), {
    method: 'DELETE'})
    .then(response => {
      if(response.ok){
        card.remove();
      }
    })
  }
})

